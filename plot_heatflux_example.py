# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 17:41:40 2018
V3.6.0
@author: holn
"""
import numpy as np
import downloadversionIRdata as IR
import matplotlib.pyplot as plt
import datetime

port = 51 # 20
program = "20180814.024"#"20171207.027"#"20181016.023"#"20180927.017"#"20180904.007"#"20171207.022" # "20181004.012"

profile = "TM3h_5_7"

if __name__=='__main__':
    
    #%% loading data
    print(datetime.datetime.now(),"start")
    status,times,images=IR.download_heatflux_by_program(port,program,time_window=[0,2],version=2,verbose=5,testmode=False,caching=True)
    print('done')
    
    #%% plotting data
    if status:
        status2,time,s,q=IR.extract_heatflux_profile_from_DL((np.asarray(times)-times[0])/1e9,images,profile=profile,time_window=[0.5,1.5],verbose=5,testmode=False)        
        plt.figure()
        plt.imshow(images[0]/1e6)
        plt.clim([0,1])
        plt.tick_params(axis='both',       # changes apply to the x-axis
                        which='both',      # both major and minor ticks are affected
                        bottom='off',      # ticks along the bottom edge are off
                        top='off',         # ticks along the top edge are off
                        left='off',
                        right='off',
                        labelleft='off',
                        labelbottom='off') # labels along the bottom edge are off
        c_ax = plt.colorbar()
        c_ax.set_label('q [MW/m2]')
        plt.savefig(program+' - '+str(round((np.asarray(times[-1])-times[0])/1e9,2))+' - AEF{0}IR - in MWm2.png'.format(port), dpi=300, bbox_inches='tight')
        if len(np.shape(q))==1 or np.shape(q)[0]==1:
            if np.shape(q)[0]==1:
                q=q[0]
            plt.figure()
            plt.plot(s,q/1e6,'ko-')
            plt.xlabel("s [m]", fontsize=26)
            plt.ylabel("q [MW/m2]", fontsize=26)
            plt.tick_params(labelsize=26)
            plt.title("{0} heat flux profile for {1}, finger {2}, profile {3} @1s".format(program,profile.split("_")[0],profile.split("_")[1],profile.split("_")[2]),fontsize=26)
            plt.grid()
            plt.ylim(bottom = 0)
            plt.show()
        elif len(np.shape(q)) == 2:
            plt.figure()
            X,Y=np.meshgrid(time,s)
            plt.pcolor(X,Y,q.swapaxes(0,1)/1e6,vmin=0)
            plt.xlabel("time [s]",fontsize=26)
            plt.ylabel("s [m]",fontsize=26)
            plt.tick_params(labelsize=26)
            cb=plt.colorbar()
            cb.set_label('heatflux in MW/m²', rotation=270, labelpad=15,fontsize=26)
            cb.ax.tick_params(labelsize=26)
            plt.title("{0} heat flux profile for {1}, finger {2}, profile {3} @1s".format(program,profile.split("_")[0],profile.split("_")[1],profile.split("_")[2]),fontsize=26)
            plt.show()
#        status3,mapping=IR.download_heatflux_scene_model_reference(port,program=program,verbose=0)
#        bild=np.zeros((768,1024))
        