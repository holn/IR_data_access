from setuptools import setup, find_packages

setup(
	name = 'ir-data-access',
	version = '3.6.0',
	author = 'Holger Niemann, Peter Drewelow',
	author_email = 'holger.niemann@ipp.mpg.de',
	description = 'Access Frontend for IR camera data',
	py_modules = [
                'downloadversionIRdata',
                'IR_image_tools',
                'IR_da_config_constants',
                'IR_image_tools',
                'plot_IR_data'       ,
                'Create_HDF5_from_archive_data',
                'plot_heatflux_example'
        ],
      data_files=[
                  ('',['download_config','CHANGELOG','ToDo.txt','README.md']),
                  ('data',['data/AEF10_coldframes_background_fails_real.txt',
                           'data/AEF11_coldframes_background_fails_real.txt',
                           'data/AEF20_coldframes_background_fails_real.txt',
                           'data/AEF21_coldframes_background_fails_real.txt',
                           'data/AEF30_coldframes_background_fails_real.txt',
                           'data/AEF31_coldframes_background_fails_real.txt',
                           'data/AEF40_coldframes_background_fails_real.txt',
                           'data/AEF41_coldframes_background_fails_real.txt',
                           'data/AEF51_coldframes_background_fails_real.txt',
                           'data/finger_info_HHF.csv',
                           'data/finger_info_TDU.csv'])
                  ],
	install_requires = [
		'w7xarchive>=0.11.21'
	]
);

