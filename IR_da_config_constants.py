# -*- coding: utf-8 -*-
"""
Created on Tue Jun  5 09:26:06 2018
Version: 3.6.1
@author: holn
constants and config parameters
"""
#%% Paths
calibpath="\\\\sv-e4-fs-1\\E4-Mitarbeiter\\E4 Diagnostics\\QIR\\Calibrations\\"
IRCamColdframes_fittingpath=calibpath+"IRCamColdframes_fitting\\"
#IRCamRefImagespath = calibpath+'IRCamReferenceImages\\'
IRCAMBadPixels_path = calibpath + 'IRCAMBadPixels\\'
heatflux_requestlist_path="\\\\x-drive\\Diagnostic-logbooks\\QRT-DivertorThermography\\ArchiveDB_heatflux_pipeline\\"
# get local parameter directory from script location
import os.path as ospath
parameter_file_path = ospath.join(ospath.dirname(__file__), 'data')


try:
#    import sys
    path=str(repr(__file__)).split("IR_da_config_constants")[0].split("'")[1]+"download_config"
    cFile=open(path.replace('\\\\',"\\"))#+"upload_config")
    for line in cFile:
        if line[0:3]!="###":
            if line.split("=")[0]=='archivedb':
                database = eval(line.split("=")[1].strip())
                archivepath = "http://archive-webapi.ipp-hgw.mpg.de/{0}/raw/".format(database)
            if line.split("=")[0]=='cachepath':
                cpath = database = eval(line.split("=")[1].strip())
                if cpath == "":
                    cpath = ospath.join(ospath.dirname(__file__), 'cache')
    cFile.close()    
except Exception as E:
    archivepath="http://archive-webapi.ipp-hgw.mpg.de/Test/raw/"
    print("config loading failed",E)
#    raise Exception("Config file was not loaded properly",E)
testarchivepath = "http://archive-webapi.ipp-hgw.mpg.de/Test/raw/"

#%% Dictionaries
project="W7X"
project_ana="W7XAnalysis"
portcamdict={
    'OP1.2a':{
        'AEF10': 'IRCam_Caleo768kL_0901',
        'AEF11': 'IRCam_Caleo768kL_0904',#is corrected from 0906, the cameras was swapped in the campaign in restect to the planned position
        'AEF20': 'IRCam_Caleo768kL_0702',
        'AEF21': 'IRCam_Caleo768kL_0906',#is corrected from 0904, the cameras was swapped in the campaign in restect to the planned position
        'AEF30': 'IRCam_Caleo768kL_0907',
        'AEF31': 'IRCam_Caleo768kL_0903',
        'AEF40': 'IRCam_Caleo768kL_0701',
        'AEF41': 'IRCam_Caleo768kL_0902',
        'AEF50': 'Infratec_9312907',
        'AEF51': 'IRCam_Caleo768kL_0905'},
    'OP1.2b':{
        'AEF10': 'IRCam_Caleo768kL_0901',
        'AEF11': 'IRCam_Caleo768kL_0904',#is corrected from 0906, the cameras was swapped in the campaign in restect to the planned position
        'AEF20': 'IRCam_Caleo768kL_0702',
        'AEF21': 'IRCam_Caleo768kL_0906',#is corrected from 0904, the cameras was swapped in the campaign in restect to the planned position
        'AEF30': 'IRCam_Caleo768kL_0907',
        'AEF31': 'IRCam_Caleo768kL_0903',
        'AEF40': 'IRCam_Caleo768kL_0701',
        'AEF41': 'IRCam_Caleo768kL_0902',
        'AEF50': 'Infratec_9312907',
        'AEF51': 'IRCam_Caleo768kL_0905',
        'AEK51': 'FLIR_SC8303_00037'}
    	}

portpathdict={
    'OP1.2a':{
        'AEF10': 'QRT_IRCAM/AEF10_',
        'AEF11': 'QRT_IRCAM/AEF11_',
        'AEF20': 'QRT_IRCAM/AEF20_',
        'AEF21': 'QRT_IRCAM/AEF21_',
        'AEF30': 'QRT_IRCAM/AEF30_',
        'AEF31': 'QRT_IRCAM/AEF31_',
        'AEF40': 'QRT_IRCAM/AEF40_',
        'AEF41': 'QRT_IRCAM/AEF41_',
        'AEF50': 'QRT_IRCAM/AEF50_',
        'AEF51': 'QRT_IRCAM/AEF51_'},
    'OP1.2b':{
        'AEF10': 'QRT_IRCAM/AEF10_',
        'AEF11': 'QRT_IRCAM/AEF11_',
        'AEF20': 'QRT_IRCAM/AEF20_',
        'AEF21': 'QRT_IRCAM/AEF21_',
        'AEF30': 'QRT_IRCAM/AEF30_',
        'AEF31': 'QRT_IRCAM/AEF31_',
        'AEF40': 'QRT_IRCAM/AEF40_',
        'AEF41': 'QRT_IRCAM/AEF41_',
        'AEF50': 'QRT_IRCAM/AEF50_',
        'AEF51': 'QRT_IRCAM/AEF51_',
        'AEK51': 'QSR07_FLIR/AEK51_'}
    	}
     
camlimdict={
    'IRCam_Caleo768kL_0901':
        {
        1:(50,5000),
        2:(50,3000),
        3:(50,2050),
        4:(50,1200),
        5:(50,1100),
        6:(50,850),
        7:(50,800),
        8:(50,600),
        9:(50,550)
         },
    'IRCam_Caleo768kL_0902':
        {
        1:(50,5000),
        2:(50,3000),
        3:(50,2050),
        4:(50,1200),
        5:(50,1100),
        6:(50,1000),
        7:(50,800),
        8:(50,800),
        9:(50,650)
         },
    'IRCam_Caleo768kL_0903':
        {
        1:(50,5000),
        2:(50,3000),
        3:(50,2050),
        4:(50,1200),
        5:(50,1100),
        6:(50,1000),
        7:(50,900),
        8:(50,800),
        9:(50,700)
         },
    'IRCam_Caleo768kL_0904':
        {
        1:(50,5000),
        2:(50,3000),
        3:(50,2050),
        4:(50,1200),
        5:(50,1100),
        6:(50,900),
        7:(50,800),
        8:(50,700),
        9:(50,600)
         },
    'IRCam_Caleo768kL_0905':
        {
        1:(50,5000),
        2:(50,3000),
        3:(50,2050),
        4:(50,1200),
        5:(50,1100),
        6:(50,900),
        7:(50,800),
        8:(50,700),
        9:(50,600)
         },
    'IRCam_Caleo768kL_0906':
        {
        1:(50,5000),
        2:(50,3000),
        3:(50,2050),
        4:(50,1200),
        5:(50,1000),
        6:(50,800),
        7:(50,750),
        8:(50,600),
        9:(50,550)
         },
    'IRCam_Caleo768kL_0907':
        {
        1:(50,5000),
        2:(50,3000),
        3:(50,2050),
        4:(50,1200),
        5:(50,1100),
        6:(50,900),
        7:(50,800),
        8:(50,700),
        9:(50,600)
         },
    'IRCam_Caleo768kL_0701':        
        {
        1:(50,5000),
        2:(50,3000),
        3:(50,2050),
        4:(50,1580),
        5:(50,1100),
        6:(50,900),
        7:(50,800),
        8:(50,750),
        9:(50,700)
         },
    'IRCam_Caleo768kL_0702':
        {
        1:(50,5000),
        2:(50,3000),
        3:(50,2050),
        4:(50,1580),
        5:(50,1150),
        6:(50,1000),
        7:(50,800),
        8:(50,750),
        9:(50,700)
         },
     'Infratec_9312907':
         {
             0:{
             25:(50,400),
             50:(50,300),
            100:(50,250),
            200:(50,200),
            400:(50,150),
            800:(50,100)
             },
             1:{
             25:(300,1200),
             50:(200,800),
            100:(150,650),
            200:(100,500),
            400:(50,400),
            800:(50,300)
             },
            2:{
             25:(200,1200),
             50:(200,1200),
            100:(200,1200),
            200:(200,1200),
            400:(200,1000),
            800:(200,700)
             },
         }
}

valid_FOV_circle = {
    10: [562, 364, 550],#, 600],
    11: [502, 404, 520],#, 570]
    20: [535, 374, 510],
    21: [548, 404, 505],
    30: [517, 389, 520],
    31: [562, 364, 520],
    40: [542, 394, 520],
    41: [522, 394, 520],
    50: [640, 454, 620],
    51: [512, 379, 510]
    }

valid_background_rectangle = {
    10: [500,0 ,800 ,100],#10: [300,0 ,800 ,200],
    11: [50,200,170,290],#11: [200,100,700,200],
    20: [100,100,275,135],#20: [100,100,400,200],
    21: [300,0,700,75],#21: [150,0,800,200],
    30: [200,0,800,100],#30: [200,0,800,200],#
    31: [200,100,700,200],#
    40: [550,0,690,140],#
    41: [120,100,250,200],#
    50: [390,940,880,1010],#
    51: [350,0,670,120]#
    }
    
TC_port={
    10:[10],
    11:[10],
    20:[20],
    21:[20],
    30:[30],
    31:[30],
    40:[10,20,30],
    41:[10,20,30],
    50:[50],
    51:[10,20,30]
    }
TC_channel={
    10:[[66,67],["ActV1QSR10CT001","ActV1QSR10CT002"]], 
    20:[[72,73],["ActV1QSR20CT001","ActV1QSR20CT002"]],
    30:[[78,79],["ActV1QSR30CT001","ActV1QSR30CT002"]],
    50:[[90,91,84,85],["ActV1QRT50CT001","ActV1QRT50CT002","ActV1QRT50CT003","ActV1QRT50CT004"]]
    }
    
TC_database={
    50: {
        0: {'Y': -4220.44, 'Z': 966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': 2947.882, 'Targetelement': 4}, 
        1: {'Y': -4507.509, 'Z': 1000.004, 'Targetmodule': 'TM2h', 'active': True, 'X': 2673.037, 'Targetelement': 4}, 
        2: {'Y': -4834.308, 'Z': 1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': 2339.889, 'Targetelement': 5}, 
        3: {'Y': -5124.416, 'Z': 998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': 1998.605, 'Targetelement': 5}, 
        4: {'Y': -5862.386, 'Z': 749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': 430.664, 'Targetelement': 4}, 
        5: {'Y': -5971.865, 'Z': 676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': -15.031, 'Targetelement': 6}, 
        6: {'Y': -6065.916, 'Z': 542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': -697.011, 'Targetelement': 6}, 
        7: {'Y': -4068.477, 'Z': 644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': 2704.554, 'Targetelement': 5}, 
        8: {'Y': -4495.307, 'Z': 719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': 2346.682, 'Targetelement': 6}, 
        9: {'Y': -4841.007, 'Z': 771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': 1981.551, 'Targetelement': 6}, 
        10: {'Y': -5872.346, 'Z': 772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': 24.186, 'Targetelement': 0}, 
        11: {'Y': -5910.584, 'Z': 726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': 5.577, 'Targetelement': 0}, 
        12: {'Y': -4703.059, 'Z': 1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': 2309.024, 'Targetelement': 0}, 
        13: {'Y': -4945.311, 'Z': 1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': 2464.537, 'Targetelement': 0}, 
        14: {'Y': -4431.787, 'Z': 740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': 2325.89, 'Targetelement': 0}, 
        15: {'Y': -4322.24, 'Z': 714.53, 'Targetmodule': 'DR', 'active': True, 'X': 2374.085, 'Targetelement': 0}}, 
    51: {0: {'Y': -5147.13, 'Z': -966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': 95.826, 'Targetelement': 4}, 
         1: {'Y': -5217.823, 'Z': -1000.004, 'Targetmodule': 'TM2h', 'active': True, 'X': 486.915, 'Targetelement': 4}, 
        2: {'Y': -5286.39, 'Z': -1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': 948.525, 'Targetelement': 5}, 
        3: {'Y': -5320.49, 'Z': -998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': 1395.15, 'Targetelement': 5}, 
        4: {'Y': -4995.908, 'Z': -749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': 3097.409, 'Targetelement': 4}, 
        5: {'Y': -4822.505, 'Z': -676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': 3522.334, 'Targetelement': 6}, 
        6: {'Y': -4497.737, 'Z': -542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': 4129.35, 'Targetelement': 6}, 
        7: {'Y': -4881.164, 'Z': -644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': 203.361, 'Targetelement': 5}, 
        8: {'Y': -5016.125, 'Z': -719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': 743.77, 'Targetelement': 6}, 
        9: {'Y': -5081.183, 'Z': -771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': 1242.364, 'Targetelement': 6}, 
        10: {'Y': -4779.849, 'Z': -772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': 3436.923, 'Targetelement': 0}, 
        11: {'Y': -4860.039, 'Z': -726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': 3494.011, 'Targetelement': 0}, 
        12: {'Y': -5163.219, 'Z': -1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': 896.724, 'Targetelement': 0}, 
        13: {'Y': -5447.878, 'Z': -1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': 912.415, 'Targetelement': 0}, 
        14: {'Y': -4893.659, 'Z': -740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': 704.131, 'Targetelement': 0}, 
        15: {'Y': -4949.37, 'Z': -714.53, 'Targetmodule': 'DR_TM2v', 'active': True, 'X': 638.444, 'Targetelement': 0}}, 
    20: {0: {'Y': 5147.13, 'Z': 966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': 95.826, 'Targetelement': 4}, 
         1: {'Y': 5217.823, 'Z': 1000.004, 'Targetmodule': 'TM2h', 'active': True, 'X': 486.915, 'Targetelement': 4}, 
        2: {'Y': 5286.39, 'Z': 1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': 948.525, 'Targetelement': 5}, 
        3: {'Y': 5320.49, 'Z': 998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': 1395.15, 'Targetelement': 5}, 
        4: {'Y': 4995.908, 'Z': 749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': 3097.409, 'Targetelement': 4}, 
        5: {'Y': 4822.505, 'Z': 676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': 3522.334, 'Targetelement': 6}, 
        6: {'Y': 4497.737, 'Z': 542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': 4129.35, 'Targetelement': 6}, 
        7: {'Y': 4881.164, 'Z': 644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': 203.361, 'Targetelement': 5}, 
        8: {'Y': 5016.125, 'Z': 719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': 743.77, 'Targetelement': 6}, 
        9: {'Y': 5081.183, 'Z': 771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': 1242.364, 'Targetelement': 6}, 
        10: {'Y': 4765.044, 'Z': 772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': 3432.112, 'Targetelement': 0}, 
        11: {'Y': 4785.041, 'Z': 726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': 3469.642, 'Targetelement': 0}, 
        12: {'Y': 5162.065, 'Z': 1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': 896.349, 'Targetelement': 0}, 
        13: {'Y': 5449.459, 'Z': 1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': 912.928, 'Targetelement': 0}, 
        14: {'Y': 4952.514, 'Z': 740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': 723.255, 'Targetelement': 0}, 
        15: {'Y': 4892.218, 'Z': 714.53, 'Targetmodule': 'DR_TM2v', 'active': True, 'X': 619.874, 'Targetelement': 0}}, 
    21: {0: {'Y': 4220.44, 'Z': -966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': 2947.882, 'Targetelement': 4}, 
         1: {'Y': 4507.509, 'Z': -1000.004, 'Targetmodule': 'TM2h', 'active': False, 'X': 2673.037, 'Targetelement': 4}, 
        2: {'Y': 4834.308, 'Z': -1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': 2339.889, 'Targetelement': 5}, 
        3: {'Y': 5124.416, 'Z': -998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': 1998.605, 'Targetelement': 5}, 
        4: {'Y': 5862.386, 'Z': -749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': 430.664, 'Targetelement': 4}, 
        5: {'Y': 5971.865, 'Z': -676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': -15.031, 'Targetelement': 6}, 
        6: {'Y': 6065.916, 'Z': -542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': -697.011, 'Targetelement': 6}, 
        7: {'Y': 4068.477, 'Z': -644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': 2704.554, 'Targetelement': 5}, 
        8: {'Y': 4495.307, 'Z': -719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': 2346.682, 'Targetelement': 6}, 
        9: {'Y': 4841.007, 'Z': -771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': 1981.551, 'Targetelement': 6}, 
        10: {'Y': 5887.151, 'Z': -772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': 28.996, 'Targetelement': 0}, 
        11: {'Y': 5985.582, 'Z': -726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': 29.945, 'Targetelement': 0}, 
        12: {'Y': 4704.213, 'Z': -1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': 2309.399, 'Targetelement': 0}, 
        13: {'Y': 4943.73, 'Z': -1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': 2464.024, 'Targetelement': 0}, 
        14: {'Y': 4372.932, 'Z': -740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': 2306.766, 'Targetelement': 0}, 
        15: {'Y': 4379.393, 'Z': -714.53, 'Targetmodule': 'DR_TM2v', 'active': True, 'X': 2392.655, 'Targetelement': 0}}, 
    40: {0: {'Y': -4107.791, 'Z': 966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': -3102.931, 'Targetelement': 4}, 
         1: {'Y': -3935.106, 'Z': 1000.004, 'Targetmodule': 'TM2h', 'active': True, 'X': -3460.882, 'Targetelement': 4}, 
        2: {'Y': -3719.25, 'Z': 1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': -3874.635, 'Targetelement': 5}, 
        3: {'Y': -3484.318, 'Z': 998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': -4256.006, 'Targetelement': 5}, 
        4: {'Y': -2221.163, 'Z': 749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': -5442.378, 'Targetelement': 4}, 
        5: {'Y': -1831.112, 'Z': 676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': -5684.226, 'Targetelement': 6}, 
        6: {'Y': -1211.574, 'Z': 542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': -5984.417, 'Targetelement': 6}, 
        7: {'Y': -3829.412, 'Z': 644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': -3033.599, 'Targetelement': 5}, 
        8: {'Y': -3620.954, 'Z': 719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': -3550.127, 'Targetelement': 6}, 
        9: {'Y': -3380.521, 'Z': 771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': -3991.738, 'Targetelement': 6}, 
        10: {'Y': -1837.657, 'Z': 772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': -5577.46, 'Targetelement': 0}, 
        11: {'Y': -1831.775, 'Z': 726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': -5619.576, 'Targetelement': 0}, 
        12: {'Y': -3649.338, 'Z': 1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': -3759.348, 'Targetelement': 0}, 
        13: {'Y': -3872.099, 'Z': 1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': -3941.686, 'Targetelement': 0}, 
        14: {'Y': -3581.55, 'Z': 740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': -3496.14, 'Targetelement': 0}, 
        15: {'Y': -3593.534, 'Z': 714.53, 'Targetmodule': 'DR_TM2v', 'active': True, 'X': -3377.062, 'Targetelement': 0}}, 
    41: {0: {'Y': -1681.686, 'Z': -966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': -4865.6, 'Targetelement': 4}, 
         1: {'Y': -2075.48, 'Z': -1000.004, 'Targetmodule': 'TM2h', 'active': True, 'X': -4811.98, 'Targetelement': 4}, 
        2: {'Y': -2535.686, 'Z': -1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': -4734.545, 'Targetelement': 5}, 
        3: {'Y': -2970.989, 'Z': -998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': -4628.962, 'Targetelement': 5}, 
        4: {'Y': -4489.632, 'Z': -749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': -3794.239, 'Targetelement': 4}, 
        5: {'Y': -4840.175, 'Z': -676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': -3498.014, 'Targetelement': 6}, 
        6: {'Y': -5317.122, 'Z': -542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': -3001.562, 'Targetelement': 6}, 
        7: {'Y': -1701.77, 'Z': -644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': -4579.421, 'Targetelement': 5}, 
        8: {'Y': -2257.435, 'Z': -719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': -4540.781, 'Targetelement': 6}, 
        9: {'Y': -2751.73, 'Z': -771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': -4448.581, 'Targetelement': 6}, 
        10: {'Y': -4745.762, 'Z': -772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': -3483.839, 'Targetelement': 0}, 
        11: {'Y': -4824.836, 'Z': -726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': -3542.463, 'Targetelement': 0}, 
        12: {'Y': -2448.358, 'Z': -1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': -4633.41, 'Targetelement': 0}, 
        13: {'Y': -2551.245, 'Z': -1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': -4899.289, 'Targetelement': 0}, 
        14: {'Y': -2181.893, 'Z': -740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': -4436.558, 'Targetelement': 0}, 
        15: {'Y': -2136.636, 'Z': -714.53, 'Targetmodule': 'DR_TM2v', 'active': True, 'X': -4509.841, 'Targetelement': 0}}, 
    10: {0: {'Y': 1499.415, 'Z': 966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': 4924.823, 'Targetelement': 4}, 
         1: {'Y': 1149.312, 'Z': 1000.004, 'Targetmodule': 'TM2h', 'active': True, 'X': 5112.91, 'Targetelement': 4}, 
        2: {'Y': 731.483, 'Z': 1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': 5320.766, 'Targetelement': 5}, 
        3: {'Y': 317.255, 'Z': 998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': 5491.212, 'Targetelement': 5}, 
        4: {'Y': -1401.991, 'Z': 749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': 5708.543, 'Targetelement': 4}, 
        5: {'Y': -1859.703, 'Z': 676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': 5674.936, 'Targetelement': 6}, 
        6: {'Y': -2537.368, 'Z': 542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': 5553.641, 'Targetelement': 6}, 
        7: {'Y': 1314.955, 'Z': 644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': 4705.105, 'Targetelement': 5}, 
        8: {'Y': 842.701, 'Z': 719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': 5000.456, 'Targetelement': 6}, 
        9: {'Y': 388.614, 'Z': 771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': 5216.404, 'Targetelement': 6}, 
        10: {'Y': -1791.653, 'Z': 772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': 5592.407, 'Targetelement': 0}, 
        11: {'Y': -1821.167, 'Z': 726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': 5623.023, 'Targetelement': 0}, 
        12: {'Y': 742.687, 'Z': 1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': 5186.403, 'Targetelement': 0}, 
        13: {'Y': 815.729, 'Z': 1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': 5464.854, 'Targetelement': 0}, 
        14: {'Y': 842.555, 'Z': 740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': 4933.619, 'Targetelement': 0}, 
        15: {'Y': 922.243, 'Z': 714.53, 'Targetmodule': 'DR_TM2v', 'active': True, 'X': 4844.327, 'Targetelement': 0}}, 
    11: {0: {'Y': -1499.415, 'Z': -966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': 4924.823, 'Targetelement': 4}, 
         1: {'Y': -1149.312, 'Z': -1000.004, 'Targetmodule': 'TM2h', 'active': True, 'X': 5112.91, 'Targetelement': 4}, 
        2: {'Y': -731.483, 'Z': -1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': 5320.766, 'Targetelement': 5}, 
        3: {'Y': -317.255, 'Z': -998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': 5491.212, 'Targetelement': 5}, 
        4: {'Y': 1401.991, 'Z': -749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': 5708.543, 'Targetelement': 4}, 
        5: {'Y': 1859.703, 'Z': -676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': 5674.936, 'Targetelement': 6}, 
        6: {'Y': 2537.368, 'Z': -542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': 5553.641, 'Targetelement': 6}, 
        7: {'Y': -1314.955, 'Z': -644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': 4705.105, 'Targetelement': 5}, 
        8: {'Y': -842.701, 'Z': -719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': 5000.456, 'Targetelement': 6}, 
        9: {'Y': -388.614, 'Z': -771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': 5216.404, 'Targetelement': 6}, 
        10: {'Y': 1791.653, 'Z': -772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': 5607.974, 'Targetelement': 0}, 
        11: {'Y': 1821.167, 'Z': -726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': 5701.88, 'Targetelement': 0}, 
        12: {'Y': -742.687, 'Z': -1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': 5187.616, 'Targetelement': 0}, 
        13: {'Y': -815.729, 'Z': -1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': 5463.192, 'Targetelement': 0}, 
        14: {'Y': -842.555, 'Z': -740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': 4871.735, 'Targetelement': 0}, 
        15: {'Y': -922.243, 'Z': -714.53, 'Targetmodule': 'DR_TM2v', 'active': True, 'X': 4904.421, 'Targetelement': 0}}, 
    30: {0: {'Y': 1681.686, 'Z': 966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': -4865.6, 'Targetelement': 4}, 
         1: {'Y': 2075.48, 'Z': 1000.004, 'Targetmodule': 'TM2h', 'active': True, 'X': -4811.98, 'Targetelement': 4}, 
        2: {'Y': 2535.686, 'Z': 1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': -4734.545, 'Targetelement': 5}, 
        3: {'Y': 2970.989, 'Z': 998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': -4628.962, 'Targetelement': 5}, 
        4: {'Y': 4489.632, 'Z': 749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': -3794.239, 'Targetelement': 4}, 
        5: {'Y': 4840.175, 'Z': 676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': -3498.014, 'Targetelement': 6}, 
        6: {'Y': 5317.122, 'Z': 542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': -3001.562, 'Targetelement': 6}, 
        7: {'Y': 1701.77, 'Z': 644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': -4579.421, 'Targetelement': 5}, 
        8: {'Y': 2257.435, 'Z': 719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': -4540.781, 'Targetelement': 6}, 
        9: {'Y': 2751.73, 'Z': 771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': -4448.581, 'Targetelement': 6}, 
        10: {'Y': 4736.612, 'Z': 772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': -3471.245, 'Targetelement': 0}, 
        11: {'Y': 4778.485, 'Z': 726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': -3478.666, 'Targetelement': 0}, 
        12: {'Y': 2447.645, 'Z': 1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': -4632.429, 'Targetelement': 0}, 
        13: {'Y': 2552.222, 'Z': 1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': -4900.633, 'Targetelement': 0}, 
        14: {'Y': 2218.267, 'Z': 740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': -4486.623, 'Targetelement': 0}, 
        15: {'Y': 2101.314, 'Z': 714.53, 'Targetmodule': 'DR_TM2v', 'active': True, 'X': -4461.224, 'Targetelement': 0}}, 
    31: {0: {'Y': 4107.791, 'Z': -966.369, 'Targetmodule': 'TM1h', 'active': True, 'X': -3102.931, 'Targetelement': 4}, 
         1: {'Y': 3935.106, 'Z': -1000.004, 'Targetmodule': 'TM2h', 'active': True, 'X': -3460.882, 'Targetelement': 4}, 
        2: {'Y': 3719.25, 'Z': -1006.452, 'Targetmodule': 'TM3h', 'active': True, 'X': -3874.635, 'Targetelement': 5}, 
        3: {'Y': 3484.318, 'Z': -998.608, 'Targetmodule': 'TM4h', 'active': True, 'X': -4256.006, 'Targetelement': 5}, 
        4: {'Y': 2221.163, 'Z': -749.666, 'Targetmodule': 'TM7h', 'active': True, 'X': -5442.378, 'Targetelement': 4}, 
        5: {'Y': 1831.112, 'Z': -676.355, 'Targetmodule': 'TM8h', 'active': True, 'X': -5684.226, 'Targetelement': 6}, 
        6: {'Y': 1211.574, 'Z': -542.714, 'Targetmodule': 'TM9h', 'active': True, 'X': -5984.417, 'Targetelement': 5}, 
        7: {'Y': 3829.412, 'Z': -644.281, 'Targetmodule': 'TM1v', 'active': True, 'X': -3033.599, 'Targetelement': 5}, 
        8: {'Y': 3620.954, 'Z': -719.195, 'Targetmodule': 'TM2v', 'active': True, 'X': -3550.127, 'Targetelement': 6}, 
        9: {'Y': 3380.521, 'Z': -771.012, 'Targetmodule': 'TM3v', 'active': True, 'X': -3991.738, 'Targetelement': 6}, 
        10: {'Y': 1846.807, 'Z': -772.15, 'Targetmodule': 'DR_TM8h', 'active': True, 'X': -5590.053, 'Targetelement': 0}, 
        11: {'Y': 1878.126, 'Z': -726.881, 'Targetmodule': 'TMR_TM8h', 'active': True, 'X': -5683.373, 'Targetelement': 0}, 
        12: {'Y': 3650.051, 'Z': -1027.995, 'Targetmodule': 'TMR_TM3h', 'active': True, 'X': -3760.329, 'Targetelement': 0}, 
        13: {'Y': 3871.122, 'Z': -1136.99, 'Targetmodule': 'DR_TM3h', 'active': True, 'X': -3940.342, 'Targetelement': 0}, 
        14: {'Y': 3545.175, 'Z': -740.545, 'Targetmodule': 'TMR_TM2v', 'active': True, 'X': -3446.075, 'Targetelement': 0}, 
        15: {'Y': 3628.857, 'Z': -714.53, 'Targetmodule': 'DR_TM2v', 'active': True, 'X': -3425.679, 'Targetelement': 0}}}
from matplotlib import cm 
c2dict={
        'red'  :  ((0,0,0),(0.166, 0, 0), (0.33, 0, 0), (0.49, 0, 0), (0.66, 1, 1), (0.82, 1, 1), (1, 1, 1)),
        'green':  ((0,0,0),(0.166, 0, 0), (0.33, 1, 1), (0.49, 1, 1), (0.66, 0, 0), (0.82, 1, 1), (1, 1, 1)),
        'blue' :  ((0,0,0),(0.166, 1, 1), (0.33, 1, 1), (0.49, 0, 0), (0.66, 0, 0), (0.82, 0, 0), (1, 1, 1))
            }
c3dict=c2dict.copy()        
exJet=cm.colors.LinearSegmentedColormap('mod_Jet', c2dict, 1024)
c3dict['alpha']=((0.0,0.0,0.0),(0.33,0.99,0.99),(1.0,1.0,1.0))
exJet_trans=cm.colors.LinearSegmentedColormap('mod_Jet_trans', c3dict, 1024)
