# -*- coding: utf-8 -*-
"""
Created on Tue Dec 19 15:02:53 2017

loading IR data and printing plots

@author: pdd
"""

#import numpy as np
import datetime
import downloadversionIRdata as downIR
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    
    camera = 50
    program_str = "20171207.054"
    time_window = [0, 1]
    T_0 = 273.15 # [K] of 0°C
    
    #%% loading data
    print(datetime.datetime.now(), "start")
    status, time, images, valid, error_images = downIR.get_temp_from_raw_by_program(camera,
                                                                      program_str,
                                                                      time_window=time_window,
                                                                      emi=0.8,
                                                                      T_version=2,
                                                                      version=0,
                                                                      threads=1,
                                                                      give_ERROR=True,
                                                                      verbose=5)
    print('fertig')

    #%% plotting data

    plt.figure()
    plt.imshow(images[50]-T_0, cmap=plt.jet())
    plt.clim([0, np.max(images[50])-T_0])
    plt.tick_params(axis='both',       # changes apply to the x-axis
                    which='both',      # both major and minor ticks are affected
                    bottom='off',      # ticks along the bottom edge are off
                    top='off',         # ticks along the top edge are off
                    left='off',
                    right='off',
                    labelleft='off',
                    labelbottom='off') # labels along the bottom edge are off
    c_ax = plt.colorbar()
    c_ax.set_label('T [°C]')
    plt.title('W7-X #{0} - camera AEF{1}'.format(program_str, camera))
    save_file_name = '{0} - {1:.1f}s - AEF{2}IR - image.png'.format(program_str, time_window[0]+0.5, camera)
    plt.savefig(save_file_name, dpi=300, bbox_inches='tight')
    plt.show()

    #%% plotting time trace
    
    position = [255,505]    #  in pixel
    plt.figure()
    images = np.array(images)
    error_images = np.array(error_images)
    timetrace = images[:,position[0], position[1]]
    timetrace_error = error_images[:,position[0], position[1]]
#    plt.plot((time-time[0])/1E9+time_window[0], timetrace-T_0)
    plt.errorbar((time-time[0])/1E9+time_window[0], timetrace-T_0, yerr=timetrace_error, 
                 linestyle='', marker='.', markersize=2, color='k', capsize=3, ecolor='r')
    plt.xlabel('time [s]')
    plt.ylabel('T [°C]')
    plt.title('W7-X #{0} - camera AEF{1}'.format(program_str, camera))
    save_file_name = '{0} - pixel {1} - AEF{2}IR - timetrace.png'.format(program_str, position, camera)
    plt.savefig(save_file_name, dpi=300, bbox_inches='tight')
    plt.show()
    
    print('max. T: {0:.1f}°C at t={1}ns'.format(np.max(timetrace-T_0), time[np.argmax(timetrace)]))
